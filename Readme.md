# Gobble Ansible Role

This role installs and configures the GoBGP baselayer engine, [Gobble](https://gitlab.com/mergetb/tech/gobble)

## Variables

| name | required | default | description |
| ---- | ---------| ------- | ----------- |
| interface | **yes** | | the interface Gobble's peer router is on |
| router\_id | **yes** (when with\_gobgp=yes) | | BGP router id (e.g. 10.99.0.1) |
| router\_as | **yes** (when with\_gobgp=yes) | | BGP router AS number (e.g. 64747) |
| peer\_as | **yes** (when with\_gobgp=yes) | | peer BGP router AS number (e.g. 64701) |
